<?php
declare(strict_types=1);

namespace Common\Libraries\Database;

use Config\MySql as Config;

class MySql
{
    /** @var MySql */
    private static $instance;

    /** @var \PDO */
    private $dbh;

    /** @var \Config\MySql */
    private $config;

    /**
     * MySql constructor.
     *
     * @param Config $config
     */
    private function __construct(Config $config)
    {
        $this->initConnection();
    }

    /**
     * @return $this
     */
    private function initConnection()
    {
        try {
            $this->dbh = new \PDO(
                Config::DB_DRIVER . ':host=' . Config::DB_HOST
                . (Config::DB_PORT ? ';port=' . Config::DB_PORT : '')
                . ';dbname=' . Config::DB_NAME.';charset=' . Config::DB_CHARSET,
                Config::DB_USER,
                Config::DB_PASS,
                [
                    \PDO::ATTR_ERRMODE    => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_PERSISTENT => true,
                ]
            );
        } catch (\PDOException $e) {
            die('Database connection failed '.$e->getMessage());
        }

        return $this;
    }

    /**
     * @return MySql
     */
    public static function getInstance()
    {
        $c = (empty(__NAMESPACE__) ? '\\' : '') . __CLASS__;
        if (!(self::$instance instanceof $c)) {
            self::$instance = new $c();
        }

        return self::$instance;
    }

    /**
     * @return \PDO
     */
    public static function getConnection()
    {
        return self::getInstance()->dbh;
    }

    /**
     * @param $sql
     *
     * @return bool|null
     */
    public function query($sql)
    {
        try {
            $result = null;
            $statement = $this->dbh->prepare($sql);
            if ($statement !== false) {
                $result = $statement->execute($sql);
            }

            return $result;
        } catch (\PDOException $e) {
            echo $e->getMessage().PHP_EOL;
        }
    }

    private function __clone()
    {

    }
}
