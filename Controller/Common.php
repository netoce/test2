<?php
declare(strict_types=1);

namespace Controller;


use View\Exceptions\TemplateException;

class Common
{
    /**
     * @param string $name
     * @param array  $data
     *
     * @throws TemplateException
     */
    protected function showTemplate(string $name, array $data = []): void
    {
        $templateFile = sprintf('%s/../View/Tpl/%s.php', __DIR__, $name);
        if (!is_file($templateFile)) {
            throw new TemplateException(
                sprintf('Не найден файл шаблона %s', $templateFile)
            );
        }
        require_once __DIR__.'/../View/Layout.php';
    }
}
