<?php
declare(strict_types=1);

namespace Controller;

use Model\FeedBack\Exceptions\EmptyFormFieldEmailException;
use Model\FeedBack\Exceptions\EmptyFormFieldTextException;
use Model\FeedBack\Exceptions\EmptyFormFieldUsernameException;

class Feedback extends Common
{
    /**
     * @throws \Exception
     */
    public function index(): void
    {
        try {
            $isSubmitForm = $this->submitForm();
            $errorRequiredField = null;
        } catch (EmptyFormFieldEmailException
            | EmptyFormFieldTextException
            | EmptyFormFieldUsernameException $e) {
            $isSubmitForm = false;
            $errorRequiredField = $e;
        }

        if ($isSubmitForm) {
            header('location: index.php');
            exit();
        }

        $this->showTemplate('Feedback', ['title' => 'Форма обратной связи']);
    }

    /**
     * @return bool
     * @throws EmptyFormFieldEmailException
     * @throws EmptyFormFieldTextException
     * @throws EmptyFormFieldUsernameException
     */
    private function submitForm(): bool
    {
        if (!$this->checkFormSubmit()) {
            return false;
        }

        $feedback = new \Model\Feedback();
        $feedback->username = $this->validatePost('userName', FILTER_SANITIZE_STRING);
        $feedback->email = $this->validatePost('email', FILTER_SANITIZE_EMAIL);
        $feedback->text = $this->validateFeedbackText('feedbackText', FILTER_SANITIZE_STRING);
        $feedback->checkRequired();
        $feedback->add();

        return true;
    }

    private function add()
    {

    }

    /**
     * @return bool
     */
    private function checkFormSubmit(): bool
    {
        $isFormSubmit = filter_input(INPUT_POST, 'feedbackSubmit', FILTER_SANITIZE_SPECIAL_CHARS);

        if ($isFormSubmit === null) {
            return false;
        }

        if ($isFormSubmit !== false) {
            return true;
        }

        return $isFormSubmit;
    }

    /**
     * @param string $fieldName
     * @param int    $filter
     *
     * @return null|string
     */
    private function validatePost(string $fieldName, int $filter): ?string
    {
        $result = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

        if ($result === null || !$result) {
            return null;
        }

        return (string) $result;
    }
}
