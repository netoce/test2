<script type="application/javascript">
    function onlyEmail(e) {
        var keynum;
        if (window.event) {
            keynum = e.keyCode;
        } else if (e.which) {
            keynum = e.which;
        }
        if ((keynum > 999 && keynum < 1106) ||
            (keynum >= 32  && keynum <= 44 && keynum !== 39 && keynum !== 43) || // пробел 32, ' 39, + 43
            (keynum >= 47  && keynum <= 47) || // . 46
            (keynum >= 58  && keynum <= 63) || // @ 64
            (keynum >= 91  && keynum <= 96 && keynum !== 95) || // ` 96, _ 95
            (keynum >= 123 && keynum <= 126) || // ~ 126
            keynum === 13 ||   // enter
            keynum === 8470
        ) {
            return false;
        }
        return String.fromCharCode(keynum);
    }

    /**
     * @param input Поле, нуждающееся в проверке. Передаётся название элемента.
     * @param error Поле, в которое нужно выдать ошибку. Передаётся название элемента.
     * @param msg Сообщение об ошибке.
     * @param regexp Строковое представление регулярного выражения.
     * @return true если значение поля соответствует регулярному выражению, false - в противном случае.
     */
    function checkRegexp(input, error, msg, regexp) {
        var re = new RegExp(regexp);
        if (document.getElementById(input).value.match(re)) {
            document.getElementById(error).innerHTML = "";
            return true;
        }
        document.getElementById(error).innerHTML = msg;
        return false;
    }

    /**
     * @param input Поле, нуждающееся в проверке. Передаётся название элемента.
     * @param error Поле, в которое нужно выдать ошибку. Передаётся название элемента.
     * @param msg Сообщение об ошибке.
     * @param len Длина, меньше которой строка быть не может.
     * @return true если значение поля по длине не меньше минимального, false - в противном случае.
     */
    function checkLength(input, error, msg, len) {
        if (document.getElementById(input).value.length < len) {
            document.getElementById(error).innerHTML = msg;
            return false;
        } else {
            document.getElementById(error).innerHTML = "";
            return true;
        }
    }

    /**
     * Проверка данных формы на корректность.
     */
    function checkFormData()
    {
        var err = '';
        var correct = true;

        txt = 'Введите правильный e-mail';
        correct = checkRegexp(
            "email",
            "errEmail",
            "<span class='red'>" + txt + "</span>",
            "^[0-9a-zA-Z_\.\-]+@[0-9a-zA-Z_\.\-]+\.[a-zA-Z]+$"
        );
        if (!correct) {
            err += '- ' + txt + ';' + "\r";
        }
        txt = 'Напишите текст сообщения';
        correct = checkLength(
            "feedbackText",
            "errFeedbackText",
            "<span class='red'>" + txt + "</span>",
            2
        );
        if (!correct) {
            err += '- ' + txt + ';' + "\r";
        }

        if (err !== '') {
            alert(err);
            err = '';
            return false;
        }
        return correct;
    }
</script>

<div class="container-fluid">
    <div class="row">
        <div class="col">
            <form method="post" action="" name="feedbackForm" id="feedbackForm">
                <div>
                    <label for="username">Имя</label>
                    <input type="text" name="userName" id="userName" required placeholder="Представьтесь, пожалуйста">
                </div>
                <div id="errUsername"></div>
                <div>
                    <label for="email">E-Mail</label>
                    <input type="text" name="email" id="email" onkeypress="return onlyEmail(event);" required placeholder="Ваш E-Mail для обратной связи">
                </div>
                <div id="errEmail"></div>
                <div>
                    <label>
                        <textarea name="feedbackText" id="feedbackText" cols="100" rows="18" required></textarea>
                    </label>
                </div>
                <div id="errFeedbackText"></div>
                <div style="text-align:right;">
                    <button type="button" name="feedbackSubmit" onClick="
                        if (checkFormData()) {
                            document.getElementById('feedbackForm').submit();
                        }
                    ">
                        Отправить
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
