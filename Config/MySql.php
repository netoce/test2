<?php
declare(strict_types=1);

namespace Config;

class MySql
{
    public const DB_DRIVER = 'mysql';

    public const DB_HOST = '127.0.0.1';
    public const DB_USER = 'test';
    public const DB_PASS = 'nljksWEF@#';
    public const DB_NAME = 'test_db';

    public const DB_PORT = 3306;
    public const DB_CHARSET = 'utf8';
}
