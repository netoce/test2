<?php
declare(strict_types=1);

namespace Model;

use Common\Libraries\Database\MySql;
use Model\FeedBack\Exceptions\EmptyFormFieldEmailException;
use Model\FeedBack\Exceptions\EmptyFormFieldTextException;
use Model\FeedBack\Exceptions\EmptyFormFieldUsernameException;

class Feedback implements CommonInterface
{
    /** @var int */
    public $id;
    /** @var string */
    public $username;
    /** @var string */
    public $email;
    /** @var string */
    public $text;
    /** @var \DateTime */
    public $created;

    public static function getTableName(): string
    {
        return 'feedback';
    }

    /**
     * @throws EmptyFormFieldEmailException
     * @throws EmptyFormFieldTextException
     * @throws EmptyFormFieldUsernameException
     */
    public function checkRequired(): void
    {
        if ($this->username === null) {
            throw new EmptyFormFieldUsernameException();
        }
        if ($this->email === null) {
            throw new EmptyFormFieldEmailException();
        }
        if ($this->text === null) {
            throw new EmptyFormFieldTextException();
        }
    }

    /**
     *
     */
    public function add(): void
    {
        $keys = ['username', 'email', 'text', 'created'];
        $query = '
            INSERT INTO ' . self::getTableName() . ' ('.implode(', ',$keys).') 
            VALUES (:' . implode(', :', $keys) . ')
        ';

        $stmt = MySql::getConnection()->prepare($query);
        $data = [$this->username, $this->email, $this->text, $this->created->format('Y-m-d H:i:s')];
        $stmt->execute($data);
        $this->id = MySql::getConnection()->lastInsertId();
    }
}
