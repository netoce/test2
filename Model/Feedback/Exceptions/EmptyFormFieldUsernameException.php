<?php
declare(strict_types=1);

namespace Model\FeedBack\Exceptions;


class EmptyFormFieldUsernameException extends \Exception
{
    protected $message = 'Не заполнено имя автора обращения';
}
