<?php
declare(strict_types=1);

namespace Model\FeedBack\Exceptions;


class EmptyFormFieldEmailException extends \Exception
{
    protected $message = 'Не заполнено поле с адресом электронной почты обращающегося';
}
