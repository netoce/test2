<?php
declare(strict_types=1);

namespace Model\FeedBack\Exceptions;


class EmptyFormFieldTextException extends \Exception
{
    protected $message = 'Не заполнено обязательное поле с текстом обратной связи';
}
