<?php
declare(strict_types=1);

namespace Model;

interface CommonInterface
{
    /**
     * @return string
     */
    public static function getTableName(): string;

    /**
     * @return bool
     */
    public function checkRequired(): bool;
}
