USE test_db;

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` mediumint(8) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `text` text,
  `created` timestamp NOT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `feedback` ADD PRIMARY KEY (`id`);
ALTER TABLE `feedback` MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
