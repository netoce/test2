<?php
declare(strict_types=1);

date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL);

spl_autoload_register(function ($classname) {
    require_once __DIR__ . '/' . str_replace('\\', DIRECTORY_SEPARATOR, $classname) . '.php';
});

$controller = (new \Controller\Feedback())->index();
